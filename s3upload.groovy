pipeline {
environment {
     M2_HOME = "/usr/local/src/apache-maven"     
   }
      agent{
    label 'master'
  }
  
  options{
    buildDiscarder(logRotator(numToKeepStr: '50'))
  }
  parameters {
    string(name: 'BRANCH', defaultValue: '', description: 'Branch to run this pipeline with, defaults to release-next')
  }
  
    stages {
       
        stage ('Clone') {
		
		steps {
		checkout([$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[url: 'https://gitlab.com/saravanannathan/thanga-maven.git']]])				
		}
		}
		
		stage ('s3upload') {
		
		steps {
		writeFile file: 'test.txt', text: 'username:password'
	s3Upload consoleLogLevel: 'INFO', dontWaitForConcurrentBuildCompletion: false, entries: [[bucket: 'saro-dev-qa', excludedFile: '', flatten: false, gzipFiles: false, keepForever: false, managedArtifacts: false, noUploadOnFailure: false, selectedRegion: 'us-east-1', showDirectlyInBrowser: false, sourceFile: '*.txt', storageClass: 'STANDARD', uploadFromSlave: false, useServerSideEncryption: false]], pluginFailureResultConstraint: 'FAILURE', profileName: 'dnbs3profile', userMetadata: []
		
		}
		}		
}

}